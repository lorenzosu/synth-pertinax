This piece uses a single [Yoshimi](http://yoshimi.sourceforge.net/) instrument made with all of its sound generators (additive synth, subtractive and pad synth) and quite a few effects. The additive synth has a pitch LFO which generates the pattern. The instrument is played with a (midi) keyboard including controlling/altering filter cutoff and Q.

The complete state file for Yoshimi is provided here.

Rendering of a performance:

- FLAC (archive.org): https://archive.org/download/synth_pertinax/syper_190324_01.flac
- Soundcloud: https://soundcloud.com/lorenzosu/syper-190324-01