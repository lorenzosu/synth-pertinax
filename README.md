# Synth Pertinax

*Synth*: short for synthesizer (Oxford English dictionary)

*Pertĭnax*: Latin for 'obstinate', 'persistent'. 'Ostinato' is also a musical term defining a repeating pattern

Synth Pertinax (SyPer for short) is a project to experiment with synth pieces which are performed or even improvised, but strongly relying on- or using some sort of auto-generated patterns.

Patterns (in a very broad way), can be as simple as repeating oscillators and more complex ones (e.g. algorithmically generated). Improvisation can be through a controller, (music) keyboard, or simply interacting with software, as long as it a realtime human interaction.

This repository will collect software-related artefacts related to the project.

'Performances' (i.e. rendered/audio recordings of an improvisation) are available:

- SoundCloud: https://soundcloud.com/lorenzosu/sets/synth-pertinax 
- Archive.org: https://archive.org/details/synth_pertinax