"""
Super simple random pattern generator based on a BASE 10-element pattern.
A 0 will be interpreted as a pause
"""
import random

# Base pattern
BASE = [7, 0, 5, 2, 5, 7, 11, 7, 2, 5]

PATTERNS = []
PATTERNS.append(BASE)
HOW_MANY = 5000

for x in range(HOW_MANY):
    # If the generated pattern already exists, we generate a new one
    # until it is unique (set by the unique flag)
    unique = False
    while not unique:
        this_pattern = []
        for i in range(len(BASE)):
            num = random.choice(BASE)
            this_pattern.append(num)
        if this_pattern not in PATTERNS:
            unique = True

    PATTERNS.append(this_pattern)

# Formattig with ';' at end of line ready to be pasted into a Pd's text object
# of course one can redirect the output to a file, e.g. with >>
for p in PATTERNS:
    print(' '.join([str(i) for i in p]), end=";\n")
